angular.module('app',['directives','ngResource'])

.controller('homeController', ['$scope','palestranteService','roomService',function($scope,palestranteService,roomService){
			// $scope.speakers = palestranteService.query();
			// $scope.rooms = roomService.query();
			
			$scope.goToMaps = function(){
				window.open("https://www.google.com.br/maps/place/9%C2%B040'07.8%22S+35%C2%B043'22.3%22W/@-9.6689919,-35.7247025,17.5z/data=!4m2!3m1!1s0x0:0x0", '_blank')
			}
	
}])
.controller('headerController', ['$scope','$location','$anchorScroll',function($scope,$location,$anchorScroll){
	// $scope.speakers = palestranteService.query();
	// $scope.rooms = roomService.query();
	// // With options
	
	$scope.goTo = function(hash){
		if(hash == 'congress' || hash == 'home'){
			$anchorScroll.yOffset = 0;
		} else {
			$anchorScroll.yOffset = -460;
		}
		var newHash = hash;
		if($location.hash() !== newHash){
			$location.hash(hash);
		} else {
			$anchorScroll();	
		}
	}
}])

.factory('palestranteService', ['$resource', function($resource){
	
	return $resource('http://localhost/coaltiAPI/app/public/speaker',
			{
				'method': 'get'
			}

		);
}])

.factory('roomService', ['$resource', function($resource){
	return $resource('http://dev.api.com/room',{
		'method':'get'
	});
}]);