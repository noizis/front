angular.module('directives',[])

.directive('header', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/template/header.html'
  };
})
.directive('home', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/home.html'
  };
})
.directive('congress', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/congress.html'
  };
})
.directive('speakers', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/speakers.html'
  };
})
.directive('pogramming', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/pogramming.html'
  };
})

.directive('local', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/local.html'
  };
})
.directive('inscription', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/inscription.html'
  };
})
.directive('contact', function () {
  return {
    restrict: 'A',
    templateUrl:'pages/contact.html'
  };
})

.directive('divider', function () {
  return {
    restrict: 'A',
    template:"<div style='width: 100%; height: 100px'>&nbsp</div>"
  };
})
